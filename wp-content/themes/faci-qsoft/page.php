<?php get_header(); ?>
<!-- ==================start content body section=============== -->
<section id="contentbody">
  <div class="container">
    <div class="row">
    <!-- start left bar content -->
      <div class=" col-sm-12 col-md-12 col-lg-12">
     
        <div class="row">
          <div class="leftbar_content">


            <?php 
            	if(have_posts()):while(have_posts()):the_post();
                the_content( );
      				endwhile;
      				else : get_template_part('template-parts','none'); 
      				endif;
      			?>	
          </div>
        </div>  
      </div>
      <!-- End left bar content -->
	  
      
    </div>
  </div>
</section>
<?php get_footer(); ?>