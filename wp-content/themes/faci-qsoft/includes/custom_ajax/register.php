<?php
add_action('wp_ajax_faci_register', 'faci_register');
add_action('wp_ajax_nopriv_faci_register', 'faci_register');
function faci_register() {
	$alert = '';
	$success = '';
	global $wpdb, $PasswordHash, $current_user, $user_ID;
	
	$user_password = $wpdb->escape(trim($_POST['user_password']));
	$user_repassword = $wpdb->escape(trim($_POST['user_repassword']));
	$user_email = $wpdb->escape(trim($_POST['user_email']));
	$user_username = $wpdb->escape(trim($_POST['user_username']));
	
	if( $user_password == "" || $user_repassword == "" || $user_email == "" || $user_username == "") {
		$alert = '<p class="error">Vui lòng không bỏ trống những thông tin bắt buộc !</p>';
	} else if(!filter_var($user_email, FILTER_VALIDATE_EMAIL)) {
		$alert = '<p class="error">Địa chỉ Email không hợp lệ !</p>';
	} else if(email_exists($user_email) ) {
		$alert = '<p class="error">Địa chỉ Email đã tồn tại !</p>';
	} else if($user_password <> $user_repassword ){
		$alert = '<p class="error">Xác nhận mật khẩu không khớp !</p>';        
	} else {
		$user_id = wp_insert_user( array ('user_pass' => apply_filters('pre_user_user_pass', $user_password), 'user_login' => apply_filters('pre_user_user_login', $user_username), 'user_email' => apply_filters('pre_user_user_email', $user_email), 'role' => 'subscriber' ) );
		if( is_wp_error($user_id) ) {
			$alert = '<p class="error">Lỗi khi tạo tài khoản </p>';
		} else {
			do_action('user_register', $user_id);
			$code = sha1( $user_id . time() );
			$activation_link = add_query_arg( array( 'key' => $code, 'user' => $user_id, 'activate' =>'true','time' =>time() ), site_url('/active/') );
			$activation_link="<a href='".$activation_link."'>LINK</a>";
			add_user_meta( $user_id, 'has_to_be_activated', $code, true );
			/*----------SEND MAIL----------*/
			$body="<p style='font-weight:100;color:#444'>Name : $user_username - Email : $user_email  </p><p style='font-weight:100;color:#444'>Đăng ký tài khoản thành công ".$activation_link." !</p>";
		    if( faci_send_mail( array( $user_email , get_bloginfo('admin_email') ),"Kích hoạt tài khoản",$body) ) 
		    	$alert = '<p class="error">Đăng ký tài khoản thành công !</p>';
			
		}
		
	}
	die($alert);
} 
add_shortcode( 'faci_active_user', 'faci_active_user' );
function faci_active_user() {
    if ( isset($_GET) && $_GET['activate']=='true' ) {
        $user_id = filter_input( INPUT_GET, 'user', FILTER_VALIDATE_INT, array( 'options' => array( 'min_range' => 1 ) ) );
        if ( $user_id ) {
            // get user meta activation hash field
            $code = get_user_meta( $user_id, 'has_to_be_activated', true );
            $now=time() - 3600*24*3;
            if( filter_input( INPUT_GET, 'time' ) > $now ) echo 'Link đã hết hạn !';
            else {
            	if ( $code == filter_input( INPUT_GET, 'key' ) ) {
	                if( !delete_user_meta($user_id, 'has_to_be_activated') )  echo 'Kích hoạt tài khoản thất bại !';
	                else  echo 'Kích hoạt tài khoản thành công !';
	            }
            }
            
        }
    }
}
