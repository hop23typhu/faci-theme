<?php  
add_action('wp_ajax_faci_contact', 'faci_contact');
add_action('wp_ajax_nopriv_faci_contact', 'faci_contact');
function faci_contact() {
	/*----------VALIDATION----------*/
	//full name 
	if (isset($_POST['cname']) & $_POST['cname']!='') $cname = $_POST['cname']; else die(__("<p class='error'>Fullname is required !</p>","deeds"));
	//email
	if (isset($_POST['cemail']) & $_POST['cemail']!='') 
	{
		$cemail = $_POST['cemail']; 
		if(!is_email( $cemail )) die( __("<p class='error'>Email is not valid !</p>","deeds") );
	}
	else die( __("<p class='error'>Email is required !</p>","deeds") );

	//Message
		if (isset($_POST['ccontent']) & $_POST['ccontent']!='') $ccontent = $_POST['ccontent']; else die(__("<p class='error'>Message is required !</p>","deeds"));


	/*----------INSERT DATABASE----------*/
	$post = array(
	    'post_title'    => $cname,
	    'post_content'    => $ccontent,
	    'post_status'   => 'publish',
	    'post_type' => 'contact',
	);
	$ajax_contact_id = wp_insert_post($post);
    add_post_meta($ajax_contact_id, 'cemail', $_POST['cemail'], true);
    add_post_meta($ajax_contact_id, 'cphone', $_POST['cphone'], true);
    /*----------SEND MAIL----------*/
    $body="<p style='font-weight:100;color:#444'>Name : $cname - Email : $cemail - Phone : $_POST[cphone] </p><p style='font-weight:100;color:#444'>Message : $ccontent</p>";
    if( faci_send_mail( array( $cemail , get_bloginfo('admin_email') ),"Contact message",$body) ) 
    die( '
    	<p class="success">Success !</p>
    	<script type="text/javascript">
            $("#cform").hide("fast");
        </script>    
    ');
	else
	die( '
    	<p class="fail">fail !</p>
    	<script type="text/javascript">
            $("#cform").hide("fast");
        </script>    
    '); 
	
}