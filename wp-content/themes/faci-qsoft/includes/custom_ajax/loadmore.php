<?php  
add_action('wp_ajax_loadmore_list', 'loadmore_list');
add_action('wp_ajax_nopriv_loadmore_list', 'loadmore_list');

function loadmore_list(){
	$page = $_POST['page'];
	$title = $_POST['title'];
	$posts_per_page = $_POST['posts_per_page'];
	$order_by = $_POST['order_by'];
	$order = $_POST['order'];
	$post_type = $_POST['post_type'];

	$df_offset = 2;
	$offset = $page*$df_offset;

	$all_posts = query_posts(array('post_type' =>'post'));
	$count_posts = count($all_posts);

	query_posts(array('post_type' =>$post_type , 'posts_per_page'=>$df_offset, 'offset'=> $offset, 'orderby' => $order_by,'order'=>$order ));

	if (have_posts()):
		while (have_posts()):the_post();
			if($count_posts <= $df_offset + ($page*$df_offset)) 
			{
				get_template_part('template-parts/list','loadmore');
				echo '<!-- end -->';
			}
			else {
				get_template_part('template-parts/list','loadmore');
				echo '<!-- continue -->';
			}
		endwhile; 
	wp_reset_postdata();
	endif; 
	die(); 
}