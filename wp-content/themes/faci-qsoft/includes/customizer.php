<?php
/**
 * deeds Theme Customizer.
 *
 * @package deeds
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function deeds_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
}
add_action( 'customize_register', 'deeds_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 * Js 
 */
function deeds_customize_preview_js() {
	wp_enqueue_script( 'deeds_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'deeds_customize_preview_js' );

function faci_customize_register( $wp_customize ) {
	$wp_customize->add_section( 'faci_session' , array(
	    'title'      => 'Faci customizer',
	    'priority'   => 30,
	) );
	//1 tuỳ chỉnh gồm 2 thành phần setting , và control
	$wp_customize->add_setting( 'color_general' , array(
	    'default'     => '#000'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'control_color_general', array(
		'label'        =>'Tông màu chính',
		'section'    => 'faci_session',
		'settings'   => 'color_general',
	) ) );
	$wp_customize->add_setting( 'color_link_tag' , array(
	    'default'     => '#337ab7'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'control_color_link_tag', array(
		'label'        =>'Màu thẻ điều hướng',
		'section'    => 'faci_session',
		'settings'   => 'color_link_tag',
	) ) );

	$wp_customize->add_setting( 'author' , array(
	    'default'     => 'Lương Ba Hợp'
	) );
	$wp_customize->add_control(
		'control_fb_fanpage', 
		array(
			'label'    => 'Tác giả',
			'section'  => 'faci_session',
			'settings' => 'author',
			'type'     => 'text',
			
		)
	);
	
	//Quản lý ảnh bị lỗi
	$wp_customize->add_setting( 'img_error' , array(
	    'default'     => '',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( 
		$wp_customize, 
		'imgnotfound_control', 
		array(
			'label'      => 'Ảnh lỗi 404',
			'section'    => 'faci_session',
			'settings'   => 'img_error',
		)
	));
}
add_action( 'customize_register', 'faci_customize_register' );//thực thi hàm
