<?php
/* Contact register post type */
    
add_action('init', 'contact_register');   
function contact_register() {  
    global $themename;
    $labels = array(
        'name'               => __('Contact', 'post type general name', $themename),
        'singular_name'      => __('Contact', 'post type singular name', $themename),
        'add_new'            => __('Add New', 'Contact', $themename),
        'add_new_item'       => __('Add New Contact', $themename),
        'edit_item'          => __('Edit Contact', $themename),
        'new_item'           => __('New Contact', $themename),
        'view_item'          => __('View Contact', $themename),
        'search_items'       => __('Search Contact', $themename),
        'not_found'          =>  __('No contact have been added yet', $themename),
        'not_found_in_trash' => __('Nothing found in Trash', $themename),
        'parent_item_colon'  => ''
    );

    $args = array(  
        'labels'            => $labels,  
        'public'            => false,// hidden in front-end
        'show_ui'           => true,
        'show_in_menu'      => true,
        'show_in_nav_menus' => false,
        'rewrite'           => false,
        'supports'          => array('title', 'editor'),
        'has_archive'       => true,
        'menu_icon'         => 'dashicons-email-alt'
       );  
  
    register_post_type( 'contact' , $args );  
}
/* Contact more info display */
add_filter('manage_edit-contact_columns', 'add_new_contact_columns');
function add_new_contact_columns($columns) {
    $columns['cb'] = '<input type="checkbox" />';
    $columns['title'] = _x('Title', 'column name');
    $columns['cemail'] = __('E-mail');
    $columns['cphone'] = __('Phone');
    $columns['date'] = __('Date');
    
    return $columns;
}
// Add to admin_init function
add_action('manage_contact_posts_custom_column', 'manage_contact_columns' ,10, 2);
function manage_contact_columns($column_name, $post_ID) {
    global $post;
    switch ($column_name) {
    case 'cemail':
        echo get_post_meta($post->ID, 'cemail',true);
        break;
     case 'cphone':
        echo get_post_meta($post->ID, 'cphone',true);
        break;
    default:
        break;
    } // end switch
} 


/* Contact setting page */
add_action( 'admin_menu','contact_menus'  );
add_action( 'admin_init', 'contact_settings' );
function contact_menus() {
    add_submenu_page(
        'edit.php?post_type=contact',
        __( 'Setting contact'),
        __( 'Settings'),
        'manage_options',
        'contact-settings',
        'contact_options'
    );
}
function contact_settings(){
    register_setting( 'faci-settings-contact', 'phone' );
    register_setting( 'faci-settings-contact', 'address' );
    register_setting( 'faci-settings-contact', 'description' );
}
/**
 * Display callback for the submenu page.
 */
function contact_options() {
?>
<div class="wrap">
    <h2>Contact Settings</h2>
    <div id="poststuff" class="metabox-holder has-right-sidebar">
        <div id="side-info-column" class="inner-sidebar">
            <div id="categorydiv" class="postbox ">
                <div class="handlediv" title="Click to toggle"></div>
                <h3 class="hndle" style="color:green;">Support</h3>
                <div class="inside">
                    <p>Supporter Lương Bá Hợp</p>
                    <p>I dont know say what dose the fox say :v ring ding ding ding</p> 
                </div>
            </div>
            
            
        </div>
        <div id="post-body">
            <div id="post-body-content">
                <form method="post" action="options.php">
                    <?php settings_fields( 'faci-settings-group' ); ?>

                    <table class="form-table">
                        <tbody>
                            <tr>
                                <th scope="row"><label for="phone">Faci phone</label></th>
                                <td><input name="phone" type="text" id="phone" value="<?php echo get_option('phone')?>" class="regular-text"></td>
                            </tr>
                            <tr>
                                <th scope="row"><label for="description">Faci phone</label></th>
                                <td>
                                    <select name="description" type="text" id="description">
                                        <option>Nam </option>
                                        <option>Nữ</option>
                                        <option>Ba D</option>
                                    </select>
                            </tr>
                            <tr>
                                <th scope="row"><label for="address">Faci address</label></th>
                                <td><input name="address" type="text" id="address"  value="<?php echo get_option('address')?>" class="regular-text">
                            <p class="description" id="tagline-description">Text description for input</p></td> 
                            </tr>
                        </tbody>
                    </table>
                    <p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="<?php _e("Save changes",$themename); ?>"></p>
                </form>
            </div><!-- #post-body-content -->
        </div>
    </div>

</div>
<?php
}

?>