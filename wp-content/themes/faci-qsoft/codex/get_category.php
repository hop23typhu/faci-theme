/*-------------------------get all category --------------------------*/
<?php 
$args = array(
	'orderby' => 'name',
	'parent' => 0
	);
$categories = get_categories( $args );
foreach ( $categories as $category ) {
	echo '<a href="' . get_category_link( $category->term_id ) . '">' . $category->name . '</a><br/>';
}
?>
/*--------------------------get term by category-------------------------*/
<ul>
	<?php
	$args = array(
		'orderby' => 'name',
		'child_of' => 13,
		);
	$categories = get_categories( $args );
	foreach ( $categories as $category ) {
		echo '<li><a href="' . get_category_link( $category->term_id ) . '">' . $category->name . '</a></li>';
	}
	?>
</ul>
/*-------------------------get all taxonomy---------------------------*/
<?php 
//list terms in a given taxonomy using wp_list_categories (also useful as a widget if using a PHP Code plugin)

$taxonomy     = 'portfolios';
$orderby      = 'name'; 
$show_count   = 0;      // 1 for yes, 0 for no
$pad_counts   = 0;      // 1 for yes, 0 for no
$hierarchical = 1;      // 1 for yes, 0 for no
$title        = '';

$args = array(
	'taxonomy'     => $taxonomy,
	'orderby'      => $orderby,
	'show_count'   => $show_count,
	'pad_counts'   => $pad_counts,
	'hierarchical' => $hierarchical,
	'title_li'     => $title
	);
	?>

	<ul>
		<?php wp_list_categories( $args ); ?>
	</ul>

	/*-------------------------get term by taxonomy----------------------*/
	<?php
	$term_id = 10;
	$taxonomy_name = 'portfolios';
	$termchildren = get_term_children( $term_id, $taxonomy_name );

	echo '<ul>';
	foreach ( $termchildren as $child ) {
		$term = get_term_by( 'id', $child, $taxonomy_name );
		echo '<li><a href="' . get_term_link( $child, $taxonomy_name ) . '">' . $term->name . '</a></li>';
	}
	echo '</ul>';
	?> 
