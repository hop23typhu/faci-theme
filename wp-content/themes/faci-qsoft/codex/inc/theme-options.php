<?php
	/*
	 * Theme Options Icon URL: http://fontawesome.io/icons/
	 *
	 */
	//General
	$RS->addOptionTab(array(
		'title' => 'General',
		'name' => 'general',
		'icon' => 'dashicons-editor-quote',
		'controls' => array(
			array(
				'description' => '',
				'label' => 'Logo',
				'type' => 'image',
				'name' => 'logo'
			),			
			array(
				'description' => '',
				'label' => 'Copyright',
				'type' => 'text',
				'name' => 'copyright'
			)
		)
	));	
	//Social Networks
	$RS->addOptionTab(array(
		'title' => 'Social Networks',
		'name' => 'author',
		'icon' => 'dashicons-editor-quote',
		'controls' => array(
			array(
				'description' => '',
				'label' => 'Facebook',
				'type' => 'text',
				'name' => 'face'
			),
			array(
				'description' => '',
				'label' => 'Twitter',
				'type' => 'text',
				'name' => 'twit'
			)
		)
	));
	//Contact
	$RS->addOptionTab(array(
		'title' => 'Contact',
		'name' => 'contact',
		'icon' => 'dashicons-editor-quote',
		'controls' => array(
			array(
				'description' => '',
				'label' => 'Image Left',
				'type' => 'image',
				'name' => 'image_left'
			),
			array(
				'description' => '',
				'label' => 'Image Right',
				'type' => 'image',
				'name' => 'image_right'
			),
			array(
				'description' => '',
				'label' => 'Contact Description',
				'type' => 'text',
				'name' => 'contact_des'
			),
			array(
				'description' => '',
				'label' => 'Contact Title',
				'type' => 'text',
				'name' => 'contact_title'
			),
			array(
				'description' => '',
				'label' => 'Contact Address',
				'type' => 'text',
				'name' => 'contact_address'
			),
			array(
				'description' => '',
				'label' => 'Contact Email',
				'type' => 'text',
				'name' => 'contact_email'
			),
			array(
				'description' => '',
				'label' => 'Contact Phone',
				'type' => 'text',
				'name' => 'contact_phone'
			),
			array(
				'description' => '',
				'label' => 'Contact Map',
				'type' => 'textarea',
				'name' => 'contact_map'
			)
		)
	));
	//Extra
	$RS->addOptionTab(array(
		'title' => 'Extra Info',
		'name' => 'extra',
		'icon' => 'dashicons-editor-quote',
		'controls' => array(
			array(
				'description' => '',
				'label' => 'Title Accompishment',
				'type' => 'text',
				'name' => 'accompishment'
			),			
			array(
				'description' => '',
				'label' => 'Title Gallery',
				'type' => 'text',
				'name' => 'titlegal'
			),			
			array(
				'description' => '',
				'label' => 'Description Gallery',
				'type' => 'text',
				'name' => 'desgal'
			),			
			array(
				'description' => '',
				'label' => 'Title Crew',
				'type' => 'text',
				'name' => 'titlecrew'
			),			
			array(
				'description' => '',
				'label' => 'Description Crew',
				'type' => 'text',
				'name' => 'descrew'
			),			
			array(
				'description' => '',
				'label' => 'Info Crew',
				'type' => 'textarea',
				'name' => 'infocrew'
			),			
			array(
				'description' => '',
				'label' => 'Race Background Image',
				'type' => 'image',
				'name' => 'bgrace'
			)
		)
	));		
?>