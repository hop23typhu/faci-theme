<?php
	global $RS;
	//Action Box
	$RS->metabox(array(
	   'name' => 'actionbox',
	   'title' => 'Action Box',
	   'rules' => array(
		  'page_template' => 'template-home.php'
	   ),
	   'controls' => array(
		  array(
		   'name' => 'actionbox',
		   'label' => 'Action Box',
		   'type' => 'repeater',
		   'layout' => 'table',
		   'add_row_text' => null,
		   'min_rows' => 1,
		   'max_rows' => 4,
		   'sorting' => true,
		   'controls' => array(
			  array(
				 'label' => 'Title',
				 'name' => 'title',
				 'type'  => 'text'
			  ),
			  array(
				 'label' => 'Image',	
				 'name' => 'image',
				 'type'  => 'image'
			  ),
			  array(
				 'label' => 'Content',
				 'name' => 'content',
				 'type'  => 'textarea'
			  )
		   ),
		   'default_value' => array(),
		   'value' => ''
		  )
	   ),
	));
	//Action Box
	$RS->metabox(array(
	   'name' => 'racingbox',
	   'title' => 'Racing Box',
	   'rules' => array(
		  'page_template' => 'template-home.php'
	   ),
	   'controls' => array(
		  array(
			 'name' => 'bgracing',
			 'label'=> 'Background',
			 'type' => 'image'
		  ),
		  array(
		   'name' => 'racingbox',
		   'label' => 'Racing Box',
		   'type' => 'repeater',
		   'layout' => 'table',
		   'add_row_text' => null,
		   'min_rows' => 1,
		   'max_rows' => 3,
		   'sorting' => true,
		   'controls' => array(
			  array(
				 'label' => 'Icon',	
				 'name' => 'icon',
				 'type'  => 'image'
			  ),
			  array(
				 'label' => 'Icon Hover',	
				 'name' => 'icon_hover',
				 'type'  => 'image'
			  ),
			  array(
				 'label' => 'Title',
				 'name' => 'title',
				 'type'  => 'text'
			  ),
			  array(
				 'label' => 'Link',
				 'name' => 'link',
				 'type'  => 'text'
			  ),
			  array(
				 'label' => 'Content',
				 'name' => 'content',
				 'type'  => 'textarea'
			  )
		   ),
		   'default_value' => array(),
		   'value' => ''
		  )
	   ),
	));
	//Accompishment
	$RS->metabox(array(
	   'name' => 'accomlink',
	   'title' => 'Accompishment Link',
	   'layout' => 'none',
	   'rules' => array(
		  'post_type' => 'accompishment'
	   ),
	   'context' => 'normal',
	   'priority' => 'default',
	   'controls' => array(
		  array(
			 'name' => 'accomlink',
			 'label'=> 'Link',
			 'type' => 'text'
		  )
	   )
	));
	//Crew
	$RS->metabox(array(
	   'name' => 'crew',
	   'title' => 'Info Crew',
	   'layout' => 'none',
	   'rules' => array(
		  'post_type' => 'crew'
	   ),
	   'context' => 'normal',
	   'priority' => 'default',
	   'controls' => array(
		  array(
			 'name' => 'job',
			 'label'=> 'Job',
			 'type' => 'text'
		  ),
		  array(
			 'name' => 'des',
			 'label'=> 'Short Description',
			 'type' => 'textarea'
		  ),
		  array(
		   'name' => 'social',
		   'label' => 'Social Crew',
		   'type' => 'group',
		   'layout' => 'row',
		   'show_header' => true,
		   'controls' => array(
			  array(
				 'name'      => 'face',
				 'label'    => 'Facebook',
				 'type'      => 'text',
			  ),
			  array(
				 'name'      => 'twit',
				 'label'    => 'Twitter',
				 'type'      => 'text',
			  ),
			  array(
				 'name'      => 'plus',
				 'label'    => 'Google Plus',
				 'type'      => 'text',
			  ),
			  array(
				 'name'      => 'inst',
				 'label'    => 'Instagram',
				 'type'      => 'text',
			  )
		   ),
		   'default_value' => array(),
		   'value' => ''
		)
	   )
	));
	//Race
	$RS->metabox(array(
	   'name' => 'inforace',
	   'title' => 'Info Race',
	   'layout' => 'none',
	   'rules' => array(
		  'post_type' => 'race'
	   ),
	   'context' => 'normal',
	   'priority' => 'default',
	   'controls' => array(
		  array(
			 'name' => 'linkrace',
			 'label'=> 'Link',
			 'type' => 'text'
		  ),
		  array(
			 'name' => 'location',
			 'label'=> 'Location',
			 'type' => 'text'
		  ),
		  array(
			 'name' => 'desrace',
			 'label'=> 'Description',
			 'type' => 'text'
		  ),
		  array(
		     'name' => 'eventtime',
		     'label' => 'Event Timer',
		     'type' => 'datetime', // date | time | datetime
		     'date_format' => 'yy-mm-dd',
		     'time_options' => array(),
		     'value' => ''
		  ),
	   )
	));
?>