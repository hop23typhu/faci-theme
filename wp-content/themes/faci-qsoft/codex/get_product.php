//http://docs.woothemes.com/document/woocommerce-shortcodes/
<!---------------------------------- Slider img link ------------------------------------>
cai dat adf-repeater trong adf

<ul class="slides">
	<?php $slider_footer = get_field("slider_footer"); ?>
	<?php 
	foreach ($slider_footer as $key => $value) :
		?>
	<li>
		<a href="<?php echo $value['link_footer'] ?>">
			<img src="<?php echo $value['img_footer']['url'] ?>" alt="" />
		</a>
	</li>
	<?php
	endforeach;
	?>
</ul>

<!---------------------------------- get featured product ------------------------------------>

<div class="feature-header">
	<?php 
	$args = array(
		'post_type' => 'product',
		'orderby'  =>'id',
		'posts_per_page' => 3,
		'meta_key' => '_featured', 
		'meta_value'=>'yes'
		);
	$feature = new WP_Query($args);
	if($feature->have_posts()) : 
		while($feature->have_posts()) : 
			$feature->the_post();
		?>
		<section id="feature-box" class="col-md-4">
			<div class="feature-item">
				<div class="feature-view">
					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('product',array('class'=>'product-img','container'=>'')); ?></a>
				</div>
				<div class="feature-description">
					<div class="row">
						<div class="col-md-12 des">
							<h3>
								<a href="<?php the_permalink(); ?>"><p><?php the_title(); ?></p></a>
							</h3>
							<p><?php echo the_excerpt(); ?></p>
						</div>
					</div>
				</div>
			</div><!--  end feature-item -->
		</section>
		<?php
		endwhile;
		else: 
			endif;
		?>
		<div class="clearfix"></div>
	</div>

	<!---------------------------------- get new product ------------------------------------>

	<div class="new-product woocommerce">
		<ul class="products row">
			<?php 
			$args = array(  
				'post_type' => 'product',
				'orderby'=>'post_date',
				'order'=>'desc',
				'posts_per_page' => '8',
				);  
			$featured_query = new WP_Query( $args );  
			if ($featured_query->have_posts()) :   
				while ($featured_query->have_posts()) : $featured_query->the_post();  
			$product = get_product( $featured_query->post->ID );?> 
			<?php wc_get_template_part( 'content', 'product' ); ?>
		<?php endwhile;  endif;  
		wp_reset_query();
		?>
	</ul>
</div>

<!---------------------------------- get product by category ------------------------------------>

<div id="product" class="col-md-12" >
	<h1 class="pad-0">4 Men</h1>
	<ul class="col-md-12 pad-0">
		<?php
		$dem=0;
		$args = array('posts_per_page' => 8,
			'product_cat'=>'dong-ho-nam',);
		$loop = new WP_Query( $args );
		    //echo "<pre>"; var_dump($loop); echo "</pre>";
		while ( $loop->have_posts() ) : $loop->the_post(); global $product; 
		$sale = get_post_meta( get_the_ID(), '_sale_price', true);//tra ve gia thuong?>
		<li class="col-md-3">
			<a href="<?php echo get_permalink( $loop->post->ID ) ?>" title="<?php echo esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID); ?>">
				<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) )?>"/>
				<div class="thongtinsp">
					<br/>
					<span class="giasp">Giá: <?php echo $product->get_price_html(); ?></span></br>
					<span class="tensp">Tên: <?php echo ''.get_the_title(); ?></span></br>
					<span class="chitiet">Chi tiết</span>
				</div>
			</a>	      		 
		</li>
	<?php endwhile;  ?>
	<?php wp_reset_query(); ?> 
</ul>
<div class="more">           
	<p><a href="<?php echo bloginfo('home');?>/?product_cat=dong-ho-nam"><span class="a17">xem tiếp</span><span class="glyphicon glyphicon-share-alt a16"></span></a>
	</p>            
</div>
</div>


<!---------------------------------- footer ------------------------------------>

<p>© <?php echo date('Y'); ?> <?php bloginfo( 'sitename' ); ?>. <?php _e('All rights reserved', 'DungHoang'); ?>. <?php _e('This website is proundly to use WordPress', 'DungHoang'); ?></p>