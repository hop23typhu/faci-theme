<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <base href="<?php bloginfo('wpurl'); ?>/" >
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <!-- Bootstrap -->
    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">

    <!-- for fontawesome icon css file -->
    <link href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css" rel="stylesheet">

    <!-- for content animate css file -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/animate.css"> 
    <link href='http://fonts.googleapis.com/css?family=Varela' rel='stylesheet' type='text/css'>
    <!-- main site css file -->
    <link href="<?php echo get_template_directory_uri(); ?>/style.css" rel="stylesheet">

 

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
  </head>
  <body>
     <!-- Preloader -->
    <div id="preloader">
      <div id="status">&nbsp;</div>
    </div>
    <!-- End Preloader -->

    <!-- start top add banner -->
      <div class="topadd_place">
        <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/addbanner_728x90_V1.jpg" alt="img"></a>
      </div>
      <!-- End top add banner -->
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
    <!-- ==================start header=============== -->
    <header id="header">
      <div class="container">
         <!-- Static navbar -->
      <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.html"><span>Color</span> Mag</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav custom_nav">
              <li class=""><a href="#">Home</a></li>              
               <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Jobs</a>
                <ul class="dropdown-menu" role="menu">
                  <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" href="#">Jobs Home</a>
                     <!--  <ul class="dropdown-menu" role="menu">
                        <li>Two Columns</li>
                        <li>Three Columns</li>
                        <li>Four Columns</li>
                      </ul>   -->
                  </li>
                  <li><a href="#">Faq</a></li>
                  <li><a href="#">Home</a></li>
                  <li><a href="#">Article</a></li>
                
                </ul>
              </li>            
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Features</a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Standard Blog Layout</a></li>
                  <li><a href="#">Post With Comments</a></li>
                  <li><a href="#">Page:Right Sidebar</a></li>                
                </ul>
              </li>
              <li><a href="#">Shortcodes</a></li>
              <li><a href="#">Archive</a></li>
              <li><a href="#">Contact</a></li>
              <li><a href="#">Download Template</a></li>
            </ul>           
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
      <form id="searchForm">
        <input type="text" placeholder="Search...">
        <input type="submit" value="">
      </form>
      </div>
    </header>    
    <!-- ==================End header=============== -->